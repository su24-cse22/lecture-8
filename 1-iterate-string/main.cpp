#include <iostream>
using namespace std;

int main() {

    /*
        Print each individual character of the string.

        Example:

        C
        O
        M
        P
        U
        T
        E
        R
    */

    string str = "COMPUTER";

    // cout << str[0] << endl;
    // cout << str[1] << endl;
    // cout << str[2] << endl;
    // cout << str[3] << endl;
    // cout << str[4] << endl;
    // cout << str[5] << endl;
    // cout << str[6] << endl;
    // cout << str[7] << endl;

    for (int i = 0; i < str.length(); i++) {
        cout << str[i] << endl;
    }

    return 0;
}