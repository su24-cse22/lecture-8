#include <iostream>
using namespace std;

bool isSpaceOrPunctuation(char c) {
    if (c == ' ' || c == ',' || c == '.' || c == '!' || c == '?') {
        return true;
    } else {
        return false;
    }
}

string findLongestWord(string text) {
    string longestWord = "";

    string word = "";
    for (int i = 0; i < text.length(); i++) {
        if (isSpaceOrPunctuation(text[i])) {
            if (word.length() > longestWord.length()) {
                longestWord = word;
            }
            word = "";

        } else {
            word += text[i];
        }
    }

    if (word.length() > longestWord.length()) {
        longestWord = word;
    }
 
    return longestWord;
}

int main() {

    /*
        Create a function that finds the longest word in a string.
        If there are multiple (longest) words, return the first one.

        Example:
        
        Text: Welcome to University of California, Merced.
        Longest Word: University
    */

    string text = "Welcome to California";
    string longestWord = findLongestWord(text);

    cout << "Text: " << text << endl;
    cout << "Longest Word: " << longestWord << endl;

    return 0;
}