#include <iostream>
using namespace std;

int main() {

    /*
        Print a dash '-' between every character of the string.
        Do not modify the original string.

        Example:

        Original: COMPUTER
        Modified: C-O-M-P-U-T-E-R
    */

    string str = "COMPUTER";
    
    for (int i = 0; i < str.length(); i++) {
        cout << str[i];

        // print dash as long as you are not looking at the last character
        if (i < str.length() - 1) {
            cout << "-";
        }
    }
    cout << endl;

    return 0;
}