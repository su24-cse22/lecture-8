#include <iostream>
using namespace std;

bool isVowel(char c) {
    c = toupper(c);
    
    if (c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U') {
        return true;
    } else {
        return false;
    }
}

string removeVowels(string text) {
    string modified = "";

    for (int i = 0; i < text.length(); i++) {
        if (!isVowel(text[i])) {
            modified += text[i];
        }
    }

    return modified;
}

int main() {

    /*
        Create a function that removes all the vowels from a string.
        Do not modify the original string.

        Example:

        Original: Welcome to UC Merced
        Modified: Wlcm t C Mrcd
    */

    string original = "Welcome to UC Merced";
    string modified = removeVowels(original);

    cout << "Original: " << original << endl;
    cout << "Modified: " << modified << endl;

    return 0;
}