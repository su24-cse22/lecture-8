#include <iostream>
using namespace std;

int main() {

    /*
        Convert a string into a funky string.
        You may modify the original string.
        
        Example: 

        Original: COMPUTER
        Modified: cOmPuTeR
    */

    string original = "computer";
    cout << "Original: " << original << endl;

    for (int i = 0; i < original.length(); i++) {
        if (i % 2 == 0) {
            original[i] = tolower(original[i]);
        } else {
            original[i] = toupper(original[i]);
        }
    }

    cout << "Modified: " << original << endl;

    return 0;
}